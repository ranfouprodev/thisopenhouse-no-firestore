import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'routes/routes.dart';

void main() => runApp(new ThisOpenHouseApp());

class ThisOpenHouseApp extends StatelessWidget {
  // App root
  @override
  Widget build(BuildContext context) {
        SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight
      ]);
    return new MaterialApp(
      title: 'This Open House Login',
      onGenerateRoute: Routes().routes,
    );
  }
}