import 'package:flutter/material.dart';
import '../auth-firebase/auth.dart';
import 'login.dart';
import 'home.dart';


class TransitionScreen extends StatefulWidget {
  TransitionScreen({Key key, this.auth}) : super(key: key);
  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => new _TransitionScreenState();
}

enum AuthStatus {
  notSignedIn,
  signedIn,
}

class _TransitionScreenState extends State<TransitionScreen> {

  AuthStatus authStatus = AuthStatus.notSignedIn;

  initState() {
    super.initState();
    widget.auth.currentUser().then((userId) {
      setState(() {
        authStatus = userId != null ? AuthStatus.signedIn : AuthStatus.notSignedIn;
      });
    });
  }

  void _updateAuthStatus(AuthStatus status) {
    setState(() {
      authStatus = status;
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.notSignedIn:
        return new LoginScreen(
          title: 'This Open House Login',
          auth: widget.auth,
          onSignIn: () => _updateAuthStatus(AuthStatus.signedIn),
        );
      case AuthStatus.signedIn:
        return new HomeScreen(
          auth: widget.auth,
          onSignOut: () => _updateAuthStatus(AuthStatus.notSignedIn)
        );
    }
    return null; // just to fix widget never returns error, in the ide
  }
}