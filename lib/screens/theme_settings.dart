import 'package:flutter/material.dart';
import '../custom-ui/rectangular_button.dart';

class ThemeSettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-3.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
        child: new Column(
          children: buildWidgets(context),
        ),
      ),
    );
  }

  List<Widget> buildWidgets(context) {
    return [
      new Container(
          padding: new EdgeInsets.only(left: 98.0, top: 31.0),
          child: constructScreen(context)
      )
    ];
  }

  Column constructScreen(context){
    return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [     
            topRightJustifiedBackLink(context),
            new Container(margin: const EdgeInsets.only(bottom: 32.0)),
           
            titleText(),
            new Container(margin: const EdgeInsets.only(bottom: 107.0)),
            
            realtorBrokeragePicButtonsRow(context),
            new Container(margin: const EdgeInsets.only(bottom: 20.0)),

            themeColorConfirmButtonsRow(context)
          ]
        );
  }

  /// UI helpers
  Row topRightJustifiedBackLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pop(context);
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Back',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
  }

  Row titleText(){
    return  Row(
              children: 
              [
                new Text(
                  'Theme Settings',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 50.0,
                      fontFamily: 'OpenSans-Regular'),
                ),
              ],
            );
  }

  Row realtorBrokeragePicButtonsRow(context){
    return Row(
          children: 
          [
          new RectangularButton(
            key: new Key('upload_realtor_pic'),
            text: 'Upload Realtor Picture',
            onPressed: (){},
            opaque: false,
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 20.0)),

          new RectangularButton(
            key: new Key('upload_brokerage_logo'),
            text: 'Upload Brokerage Logo',
            onPressed: (){},
            opaque: false,
          )    
          ],
          );
  }

  Row themeColorConfirmButtonsRow(context){
    return Row(
          children: 
          [
          new RectangularButton(
            key: new Key('theme_colour'),
            text: 'Choose Theme Colour',
            onPressed: (){},
            opaque: false,
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 20.0)),

          new RectangularButton(
            key: new Key('signout'),
            text: 'Confirm',
            onPressed: (){},
            opaque: true,
          )    
          ],
          );
  }
}
