import 'package:flutter/material.dart';
import '../custom-ui/rectangular_button.dart';
import '../custom-ui/square_button.dart';
import '../custom-ui/custom_label.dart';

class AnalyticsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: 
      new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-3.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
        child: new SingleChildScrollView(
      child: new Column(
          children: buildWidgets(context),
        )),
      ),
    );
  }

  List<Widget> buildWidgets(context) {
    return [
      new Container(
          padding: new EdgeInsets.only(left: 49.0, top: 31.0, bottom: 200.0),
          child: constructScreen(context)
      )
    ];
  }

  
  Column constructScreen(context){
    return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [     
            topRightJustifiedBackLink(context),
            new Container(margin: const EdgeInsets.only(bottom: 32.0)),
           
            titleText(),
        
            new Container(margin: const EdgeInsets.only(bottom: 58.0)),
            
            profileAndSignupsRow(context),
            new Container(margin: const EdgeInsets.only(bottom: 20.0)),

            deliveredOpenedClickedContactsDownloadRow(context)
          ]
        );
  }

  /// UI helpers
  Row topRightJustifiedBackLink (context){
    return Row(
           children: 
            [
              new Expanded(
              child: new InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
              padding: new EdgeInsets.only(right: 96.0),
              child: Text('Back',
                textAlign: TextAlign.right,
              style: new TextStyle(color: Colors.white,fontSize: 20.0,fontFamily: 'OpenSans-Bold'),
                ),
              ),
            )
          )//children wrapped in 
       ],//end children of Row
    );
  }

  Row titleText(){
    return  Row(
              children: 
              [
                new Text('Open House Analytics',
                style: new TextStyle(
                  color: Colors.white,
                  fontSize: 50.0,
                  fontFamily: 'OpenSans-Regular'),
                ),
              ],
            );
  }

  Row profileAndSignupsRow(context){
    return Row(
          children: 
          [
          new Column(
          children: 
          [  
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),
          
          new CustomLabel(
            key: new Key('a_realtor_number'),
            text: '103-572-404'
          ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          new CustomLabel(
            key: new Key('a_address'),
            text: '303 Lancaster St.'
          ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          new CustomLabel(
            key: new Key('a_date'),
            text: '09/02/2018',
          ),
          new Container(margin: const EdgeInsets.only(bottom: 10.0))

          ]
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 60.0)),
          new RectangularButton(
            key: new Key('a_signups'),
            text: 'Sign Ups',
            asset:GraphicAsset.logo,
            onPressed: (){},
            opaque: false,
          )    
          ],// Row children
          );
  }

  Row deliveredOpenedClickedContactsDownloadRow(context){
    return Row(
          children: 
          [
          new SquareButton(
            key: new Key('delivered'),
            text: 'Emails Delivered',
            onPressed: (){},
            opaque: false,
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 20.0)),
          new SquareButton(
            key: new Key('opened'),
            text: 'Emails Opened',
            onPressed: (){},
            opaque: false,
          ), 

          new Container(margin: const EdgeInsets.only(right: 20.0)),
          new SquareButton(
            key: new Key('clicked'),
            text: 'Emails Clicked',
            onPressed: (){},
            opaque: false,
          ),

          new Container(margin: const EdgeInsets.only(right: 20.0)),
          new SquareButton(
            key: new Key('download_contacts'),
            text: 'Download Contacts',
            onPressed: (){},
            opaque: true,
          ) 

          ],
          );
  }
}
