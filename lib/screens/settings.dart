import 'package:flutter/material.dart';
import '../custom-ui/rectangular_button.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-4.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
        child: new SingleChildScrollView(
      child: new Column(
          children: buildWidgets(context),
        )),
      ),
    );
  }

  List<Widget> buildWidgets(context) {
    return [
      new Container(
          padding: new EdgeInsets.only(left: 49.0, top: 31.0, bottom: 200.0),
          child: constructScreen(context)
      )
    ];
  }

  Column constructScreen(context){
    return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [     
            topRightJustifiedBackLink(context),
            new Container(margin: const EdgeInsets.only(bottom: 32.0)),
           
            titleText(),
            new Container(margin: const EdgeInsets.only(bottom: 107.0)),
            
            realtorBrokeragePicButtonsRow(context),
            new Container(margin: const EdgeInsets.only(bottom: 20.0)),

            themeColorSignoutButtonsRow(context)
          ]
        );
  }

  /// UI helpers
  Row topRightJustifiedBackLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pop(context);
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Back',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
  }

  Row titleText(){
    return  Row(
              children: 
              [
                new Text(
                  'Settings',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 50.0,
                      fontFamily: 'OpenSans-Regular'),
                ),
              ],
            );
  }

  Row realtorBrokeragePicButtonsRow(context){
    return Row(
          children: 
          [
          new RectangularButton(
            key: new Key('realtor_pic'),
            text: 'Realtor Picture',
            asset: GraphicAsset.upload,
            onPressed: (){
              Navigator.pushNamed(context, '/realtor');
            },
            opaque: false,
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 20.0)),

          new RectangularButton(
            key: new Key('brokerage_logo'),
            text: 'Brokerage Logo',
            asset: GraphicAsset.upload,
            onPressed: (){
              Navigator.pushNamed(context, '/brokerage');
            },
            opaque: false,
          )    
          ],
          );
  }

  Row themeColorSignoutButtonsRow(context){
    return Row(
          children: 
          [
          new RectangularButton(
            key: new Key('theme_colour'),
            text: 'Theme Colour',
            asset: GraphicAsset.logo,
            onPressed: (){},
            opaque: false,
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 20.0)),

          new RectangularButton(
            key: new Key('signout'),
            text: 'Sign Out',
            asset: GraphicAsset.logo,
            onPressed: (){
              Navigator.pushNamed(context, "/");
            },
            opaque: false,
          )    
          ],
          );
  }
}
