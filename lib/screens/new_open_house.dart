import 'package:flutter/material.dart';
import '../custom-ui/rectangular_button.dart';
import '../custom-ui/custom_textfield.dart';

class NewOpenHouseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-3.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
        child: new SingleChildScrollView(
      child: new Column(
          children: buildWidgets(context),
        )),
      ),
    );
  }

  List<Widget> buildWidgets(context) {
    return [
      new Container(
          padding: new EdgeInsets.only(left: 49.0, top: 31.0, bottom: 200.0),
          child: constructScreen(context)
      )
    ];
  }

  Column constructScreen(context){
    return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [     
            topRightJustifiedBackLink(context),
            new Container(margin: const EdgeInsets.only(bottom: 32.0)),
           
            titleText(),
            new Container(margin: const EdgeInsets.only(bottom: 58.0)),
            
            infoAndThemeButtonRow(context),
            new Container(margin: const EdgeInsets.only(bottom: 20.0)),

            shareAndLiveButtonsRow(context)
          ]
        );
  }

  /// UI helpers
  Row topRightJustifiedBackLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pop(context);
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Back',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
  }

  Row titleText(){
    return  Row(
              children: 
              [
                new Text(
                  'Overview',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 50.0,
                      fontFamily: 'OpenSans-Regular'),
                ),
              ],
            );
  }

  Row infoAndThemeButtonRow(context){
    return Row(
          children: 
          [
          new Column(
          children: 
          [  
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),
          new CustomTextfield(
            key: new Key('realtor_number'),
            text: 'R#',
          ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),
          new CustomTextfield(
            key: new Key('address'),
            text: 'Address',
          ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),
          new CustomTextfield(
            key: new Key('date'),
            text: 'Date',
          ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0))

            ]
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 60.0)),

          new RectangularButton(
            key: new Key('theme_settings'),
            text: 'Theme Settings',
            asset: GraphicAsset.gear,
            onPressed: (){},
            opaque: false,
          )    
          ],
          );
  }

  Row shareAndLiveButtonsRow(context){
    return Row(
          children: 
          [
          new RectangularButton(
            key: new Key('share'),
            text: 'Share With Link',
            asset: GraphicAsset.chain,
            onPressed: (){},
            opaque: false,
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 20.0)),

          new RectangularButton(
            key: new Key('live'),
            text: 'Go Live',
            asset: GraphicAsset.checkmark,
            onPressed: (){
              Navigator.pushNamed(context, '/live');
            },
            opaque: true,
          )    
          ],
          );
  }
}
