import 'package:flutter/material.dart';
import '../custom-ui/rectangular_button.dart';
import '../custom-ui/round_image.dart';

class BrokerageLogoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-4.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
        child: new Column(
          children: buildWidgets(context),
        ),
      ),
    );
  }

  List<Widget> buildWidgets(context) {
    return [
      new Container(
          padding: new EdgeInsets.only(left: 49.0, top: 31.0),
          child: constructScreen(context)
      )
    ];
  }

  Column constructScreen(context){
    return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [     
            topRightJustifiedBackLink(context),
            new Container(margin: const EdgeInsets.only(bottom: 32.0)),
           
            titleText(),
            new Container(margin: const EdgeInsets.only(bottom: 125.0)),
            
            pictureAndUploadRow(context),           
          ]
        );
  }

  /// UI helpers
  Row topRightJustifiedBackLink (context){
    return Row(
              children: 
              [
              new Expanded(
              child: new InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
              padding: new EdgeInsets.only(right: 96.0),
              child: Text('Back',
              textAlign: TextAlign.right,
              style: new TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontFamily: 'OpenSans-Bold'),
               ),
              ),
            )
          )
        ],
      );
  }

  Row titleText(){
    return  Row(
              children: 
              [
                new Text('Brokerage Logo',
                style: new TextStyle(
                  color: Colors.white,
                  fontSize: 50.0,
                  fontFamily: 'OpenSans-Regular'),
                ),
              ],
            );
  }

  Row pictureAndUploadRow(context){
    return Row(
          children: 
          [
           new Container(margin: const EdgeInsets.only(left: 56.0)),
           new RoundImage(
             key: new Key('brokerage_photo'),
             photosize: PhotoSize.small,
             url: 'https://banner2.kisspng.com/20180605/ilj/kisspng-re-max-llc-estate-agent-real-estate-re-max-nova-r-real-balloon-5b1671fce82bf7.649932921528197628951.jpg'
           ),   

          new Container(margin: const EdgeInsets.only(right: 73.0)),
          new RectangularButton(
            key: new Key('upload_logo'),
            text: 'Upload Logo',
            asset: GraphicAsset.upload,
            onPressed: (){},
            opaque: false,
          )    
          ],
          );
  }
}
