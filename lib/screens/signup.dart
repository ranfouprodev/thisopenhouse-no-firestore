import 'package:flutter/material.dart';
import '../custom-ui/custom_button.dart';
import '../custom-ui/custom_textfield.dart';

class SignupScreen extends StatelessWidget{
@override
  Widget build(BuildContext context) {

    return new Scaffold(
       body: 
        new Container(
          decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-2.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
      child: new SingleChildScrollView(
      child: new Column(
          children: buildWidgets(context),
        )),
       ),
      );
  }

  List<Widget> buildWidgets(context) {
    return [ 
        new Container (
        padding: new EdgeInsets.only(left: 98.0, top: 31.0, bottom: 240.0),
          child: constructScreen(context)
      )
    ];
  }

Column constructScreen(context){
  return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [
          topRightJustifiedBackLink(context),
          new Container(margin: const EdgeInsets.only(bottom: 93.0)),

          titletextRow1(),
          titletextRow2(), 
          new Container(margin: const EdgeInsets.only(bottom: 60.0)),

          usernameAndPasswordTextfields(),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          emailAndPhoneTextfields(),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          signupButton(context)
          ]
        );
}

 /// UI helpers
Row topRightJustifiedBackLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pop(context);
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Back',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
}

  Row titletextRow1(){
    return Row(
          children: 
          [
         new Text('Lets get you',
          style: new TextStyle(color: Colors.white ,fontSize: 50.0, fontFamily: 'OpenSans-Regular'),
        ),
          ],
          );
  }

  Row titletextRow2(){
    return  Row(
          children: 
          [
           new Text(
          'signed up',
          style: new TextStyle(color: Colors.white ,fontSize: 50.0, fontFamily: 'OpenSans-Regular'),
        ),
          ],
          );
  }

  Row usernameAndPasswordTextfields(){
    return Row(
          children: 
          [
          new CustomTextfield(
            key: new Key('username'),
            text: 'Username',
            textcolor: Colors.white,
          ),
          new Container(margin: const EdgeInsets.only(right: 20.0)),
          new CustomTextfield(
            key: new Key('password'),
            text: 'Password',
            textcolor: Colors.white,
          ) 
          ],
          );
  }

  Row emailAndPhoneTextfields(){
    return  Row(
          children: 
          [
          new CustomTextfield(
            key: new Key('email'),
            text: 'Email',
            textcolor: Colors.white,
          ),
          new Container(margin: const EdgeInsets.only(right: 20.0)),
          new CustomTextfield(
            key: new Key('phone'),
            text: 'Phone Number',
            textcolor: Colors.white,
          )
          
          ],
          );
  }

  Row signupButton(context){
    return Row(
          children: 
          [
         new CustomButton(
            key: new Key('signup'),
            text: 'Sign Up',
            textcolor: Colors.white,
            buttonwidth: ButtonWidth.long,
            opaque: true,
            onPressed: (){
              Navigator.pushNamed(context, "/main_overview");
            }
          )         
          ],
          );
  }
}
