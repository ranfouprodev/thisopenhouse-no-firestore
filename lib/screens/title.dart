import 'package:flutter/material.dart';
import '../custom-ui/custom_button.dart';

class TitleScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-1.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
        child: new SingleChildScrollView(
      child: new Column(
          children: buildWidgets(context),
        )),
      ),
    );
  }

  List<Widget> buildWidgets(context) {
    return [
      new Container(
          padding: new EdgeInsets.only(left: 98.0, top: 31.0, bottom: 160.0),
          child: constructScreen(context)
      )
    ];
  }

  Column constructScreen(context){
    return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [     
            topRightJustifiedLoginLink(context),
            new Container(margin: const EdgeInsets.only(bottom: 143.0)),
           
            titleTextRow1(),
            titleTextRow2(),
            new Container(margin: const EdgeInsets.only(bottom: 60.0)),
            
            facebookButton(context),
            new Container(margin: const EdgeInsets.only(bottom: 20.0)),

            createAccountButton(context),
            new Container(margin: const EdgeInsets.only(bottom: 133.0)),

            learnMoreLink(context)
          ]
        );
  }

  /// UI helpers
  Row topRightJustifiedLoginLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pushNamed(context, "/login");
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Log In',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
  }

  Row titleTextRow1(){
    return  Row(
              children: 
              [
                new Text(
                  'Welcome to',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 50.0,
                      fontFamily: 'OpenSans-Regular'),
                ),
              ],
            );
  }

  Row titleTextRow2(){
    return Row(
              children: 
              [
                new Text(
                  'This Open House',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 50.0,
                      fontFamily: 'OpenSans-Bold'),
                ),
              ],
            );
  }

  Row facebookButton(context){
    return Row(
              children: 
              [
                new CustomButton(
                    key: new Key('facebook'),
                    text: 'Continue with Facebook',
                    textcolor: Colors.white,
                    buttonwidth: ButtonWidth.long,
                    opaque: true,
                    onPressed: () {
                      
                      Navigator.pushNamed(context, "/main_overview");
                    }),     
              ],
            );
  }

  Row createAccountButton(context){
    return Row(
              children: 
              [
                new CustomButton(
                    key: new Key('create'),
                    text: 'Create Account',
                    textcolor: Colors.white,
                    buttonwidth: ButtonWidth.long,
                    opaque: true,
                    onPressed: () {
                      Navigator.pushNamed(context, '/signup');
                    }),
              ],
            );
  }

  Row learnMoreLink(context){
    return  Row(
              children: 
              [
                new InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, "/learn_more");
                    },
                    child: new Text(
                      'Learn More',
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Regular',
                          decoration: TextDecoration.underline),
                    )),
              ],
            );
  }

}
