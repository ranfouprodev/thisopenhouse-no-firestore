import 'package:flutter/material.dart';
import '../custom-ui/custom_button.dart';
import '../custom-ui/round_image.dart';
import '../custom-ui/custom_textfield.dart';

class GoLiveScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-5.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
        child: new SingleChildScrollView(
      child: new Column(
          children: buildWidgets(context),
        )),
      ),
    );
  }

  List<Widget> buildWidgets(context) {
    return [
      new Container(
          padding: new EdgeInsets.only(left: 49.0, top: 31.0, bottom: 200.0),
          child: constructScreen(context)
      )
    ];
  }

  Column constructScreen(context){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [
            topRightJustifiedExitLink(context),
            new Container(margin: const EdgeInsets.only(bottom: 41.0)),

            nameRow(),
            new Container(margin: const EdgeInsets.only(bottom: 24.0)),

            photoAndInfo(context)


          ]
      );
  }

  ///UI helpers
  Row topRightJustifiedExitLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pushNamed(context, "/");
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Exit',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.grey[800],
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
  }

  Row nameRow(){
    return  Row(
              children: 
              [
                new Container(margin: const EdgeInsets.only(left: 449.0)),
                new Text(
                  'Sophia Hanvold',
                  style: new TextStyle(
                      color: Colors.grey[800],
                      fontSize: 50.0,
                      fontFamily: 'OpenSans-Bold'),
                ),
              ],
            );
  }

  Row photoAndInfo(context){
    // 2 cols
    return Row(
      
          children: 
          [
          new Column(
          children: 
          [  
            new RoundImage(
            key: new Key('realtor_photo'),
            photosize: PhotoSize.large,
            url: 'https://questortech.com/wp-content/uploads/2018/07/placeholder-man.png'
          ),  

          ]
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 59.0)),

          clientInfoEntry(context) 
          ]
          );
  }

  Column clientInfoEntry(context){
    return Column(
          children:
          [ 
          new CustomTextfield(
            key: new Key('client_name'),
            text: 'Name',
            textcolor: Colors.black,
          ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

           new CustomTextfield(
            key: new Key('client_email'),
            text: 'Email',
            textcolor: Colors.black,
          ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

           new CustomTextfield(
            key: new Key('client_phone'),
            text: 'Phone',
            textcolor: Colors.black,
          ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          new Text(
                  'Are you working with a realtor?',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontFamily: 'OpenSans-Bold'),
                ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          new Row (children: 
          [
            new CustomButton(
            key: new Key('realtor_question_yes'),
            text: 'Yes',
            textcolor: Colors.grey[900],
            buttonwidth: ButtonWidth.short,
            opaque: false,
            onPressed: (){
              
            }
          ),

          new Container(margin: const EdgeInsets.only(right: 20.0)),
          new CustomButton(
            key: new Key('realtor_question_no'),
            text: 'No',
            textcolor: Colors.grey[900],
            buttonwidth: ButtonWidth.short,
            opaque: false,
            onPressed: (){
              
            }
          ),
          ]
          ),

          new Container(margin: const EdgeInsets.only(bottom: 20.0)),
          new Text(
                  'Are you pre-approved for a mortgage?',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontFamily: 'OpenSans-Bold'),
                ),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          new Row (children: 
          [
            new CustomButton(
            key: new Key('mortgage_question_yes'),
            text: 'Yes',
            textcolor: Colors.grey[900],
            buttonwidth: ButtonWidth.short,
            opaque: false,
            onPressed: (){
              
            }
          ),

          new Container(margin: const EdgeInsets.only(right: 20.0)),
          new CustomButton(
            key: new Key('mortgage_question_no'),
            text: 'No',
            textcolor: Colors.grey[900],
            buttonwidth: ButtonWidth.short,
            opaque: false,
            onPressed: (){
              
            }
          ),
          ]
          ),

          new Container(margin: const EdgeInsets.only(bottom: 20.0)),
          new CustomButton(
            key: new Key('get_sheet'),
            text: 'Get Feature Sheet',
            textcolor: Colors.grey[900],
            buttonwidth: ButtonWidth.long,
            opaque: false,
            onPressed: (){
              
            }
          ),
          ],
        );
  }

}