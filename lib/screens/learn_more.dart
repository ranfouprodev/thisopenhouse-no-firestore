import 'package:flutter/material.dart';

class LearnMoreScreen extends StatelessWidget {

final Color c = const Color.fromARGB(0xFF, 0x4D, 0x59, 0x6F);

Widget build(BuildContext context) {

  return new Scaffold(
    backgroundColor: c,
      appBar: new AppBar(
        backgroundColor: c,
        actions: <Widget>[
        ],
      ),
      body: new Center(
        child: new Text(
          'Glad you are interested in knowing more!',
          style: new TextStyle(fontSize: 50.0, color: Colors.white),
        ),
      )
    );

}

}