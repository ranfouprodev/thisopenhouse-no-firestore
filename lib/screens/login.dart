import 'package:flutter/material.dart';
import '../custom-ui/custom_button.dart';
import '../custom-ui/custom_textfield.dart';

class LoginScreen extends StatelessWidget{

@override
  Widget build(BuildContext context) {
    return new Scaffold(
       body: 
        Container(
          decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-2.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
      child: new SingleChildScrollView(
      child: Column(
          children: 
            buildWidgets(context),
            ),
       )),
      );
  }

  List<Widget> buildWidgets(context) {
    return [ new Container (
          padding: new EdgeInsets.only(left: 98.0, top: 31.0, bottom: 200.0),
          child: constructScreen(context)
      )
    ];
  }

  Column constructScreen(context){
    return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children:        
          [
          topRightJustifiedForgotPasswordLink(context),
          new Container(margin: const EdgeInsets.only(bottom: 25.0)),

          topRightJustifiedBackLink(context),
          new Container(margin: const EdgeInsets.only(bottom: 93.0)),

          titleTextRow1(),
          titleTextRow2(),
          new Container(margin: const EdgeInsets.only(bottom: 60.0)),
      
          username(),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          password(),
          new Container(margin: const EdgeInsets.only(bottom: 20.0)),

          loginButton(context)
         ]
       );
  }

  /// UI helpers
  Row topRightJustifiedForgotPasswordLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pushNamed(context, "/forgot_password");
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Forgot Password',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
  }

  Row topRightJustifiedBackLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pop(context);
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Back',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
  }

  Row  titleTextRow1(){
    return Row(
          children: 
          [
          new Text('Nice to see',
          style: new TextStyle(color: Colors.white ,fontSize: 50.0, fontFamily: 'OpenSans-Regular'),
          ),
          ],
          );
  }

  Row  titleTextRow2(){
    return Row(
          children: 
          [
          new Text('you again',
          style: new TextStyle(color: Colors.white ,fontSize: 50.0, fontFamily: 'OpenSans-Regular'),
          ),
          ],
          );
  }

  Row username(){
    return  Row(
          children: 
          [
          new CustomTextfield(
            key: new Key('username'),
            text: 'Username',
            textcolor: Colors.white,
          ),
          ],
        );
  }

  Row password(){
    return  Row(
          children: 
          [
          new CustomTextfield(
            key: new Key('password'),
            text: 'Password',
            textcolor: Colors.white,
          ),
          ],
        );
  }

  Row loginButton(context){
    return Row(
          children: 
          [
          new CustomButton(
            key: new Key('login'),
            text: 'Log In',
            textcolor: Colors.white,
            buttonwidth: ButtonWidth.long,
            opaque: true,
            onPressed: (){
              Navigator.pushNamed(context, "/main_overview");
            }
          )  
          ],
        );
  }
}
      
      
          





