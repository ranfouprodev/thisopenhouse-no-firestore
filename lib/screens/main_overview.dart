import 'package:flutter/material.dart';
import '../custom-ui/rectangular_button.dart';

class MainOverviewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/BG-3.png"), //background cover image
            fit: BoxFit.cover,
          ),
        ),
      child: new Column(
          children: buildWidgets(context),
        ),
      ),
    );
  }

  List<Widget> buildWidgets(context) {
    return [
      new Container(
          padding: new EdgeInsets.only(left: 49.0, top: 31.0),
          child: constructScreen(context)
      )
    ];
  }

  Column constructScreen(context){
    return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
          children: 
          [     
            topRightJustifiedSettingsLink(context),
            new Container(margin: const EdgeInsets.only(bottom: 32.0)),
           
            titleText(),
            new Container(margin: const EdgeInsets.only(bottom: 204.0)),
            
            newAndPastOpenHouseButtonsRow(context),
            new Container(margin: const EdgeInsets.only(bottom: 20.0)),
          ]
        );
  }

  /// UI helpers
  Row topRightJustifiedSettingsLink (context){
    return Row(
              children: 
              [
                new Expanded(
                  child: new InkWell(
                  onTap: () {
                      Navigator.pushNamed(context, "/settings_main");
                    },
                  child: Container(
                    padding: new EdgeInsets.only(right: 96.0),
                    child: Text(
                      'Settings',
                      textAlign: TextAlign.right,
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'OpenSans-Bold'),
                    ),
                  ),
                )
                )
              ],
            );
  }

  Row titleText(){
    return  Row(
              children: 
              [
                new Text(
                  'Overview',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 50.0,
                      fontFamily: 'OpenSans-Regular'),
                ),
              ],
            );
  }

  Row newAndPastOpenHouseButtonsRow(context){
    return Row(
          children: 
          [
          new RectangularButton(
            key: new Key('new'),
            text: 'New Open House',
            asset: GraphicAsset.checkmark,
            onPressed: (){
              Navigator.pushNamed(context, "/profile");
            },
            opaque: true,
          ),   
          
          new Container(margin: const EdgeInsets.only(right: 20.0)),

          new RectangularButton(
            key: new Key('past'),
            text: 'Past Open Houses',
            asset: GraphicAsset.graph,
            onPressed: (){
              Navigator.pushNamed(context, '/analytics');
            },
            opaque: false,
          )    
          ],
          );
  }
}
