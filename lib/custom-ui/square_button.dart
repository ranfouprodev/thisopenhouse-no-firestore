import 'package:flutter/material.dart';

class SquareButton extends StatelessWidget {
  SquareButton({this.key, this.text, this.onPressed, this.opaque}) : super(key: key);
  final Key key;
  final String text;
  final VoidCallback onPressed;
  final bool opaque;
  final Color blue = Color.fromARGB(0xFF, 0x6A, 0xAC, 0xFF);
  final Color alpha = Colors.white.withOpacity(0.3);

  

  @override
  Widget build(BuildContext context) {
    return new ConstrainedBox(
      constraints: BoxConstraints.expand(height: 200.0, width: 190.0),
      child: new RaisedButton(
        color: opaque ? blue : alpha,
          child: new Text(text, style: new TextStyle(color: Colors.white, fontSize: 20.0, fontFamily: 'OpenSans-Bold')),
          shape: new RoundedRectangleBorder(
            side: BorderSide(color: opaque ? blue : Colors.white),
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          onPressed: onPressed),
    );
  }
}