import 'package:flutter/material.dart';

class RoundImage extends StatelessWidget {
  RoundImage({this.key, this.photosize, this.url }) : super(key: key);

  final PhotoSize photosize;
  final Key key;
  final String url;

  @override
  Widget build(BuildContext context) {
    return new Center(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                    width: photosize == PhotoSize.large ? 402.0 : 290.0,
                    height: photosize == PhotoSize.large ? 402.0 : 290.0,
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: new NetworkImage(
                                url)
                        )
                    ))
                
              ],
            )
            );
  }
}

enum PhotoSize {
  small,
  large,
}
