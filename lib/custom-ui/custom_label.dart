import 'package:flutter/material.dart';

class CustomLabel extends StatelessWidget {
  CustomLabel({this.key, this.text}) : super(key: key);
  final Key key;
  final String text;
  
  @override
  Widget build(BuildContext context) {
    return new Container(
                width: 360.0,
                child: new Text (
                    text,
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontFamily: 'OpenSans-Bold'                       
                    ),
                ),
                decoration: new BoxDecoration (
                  border: new Border.all(color: Colors.white),
                    borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
                    color: Colors.white.withOpacity(0.3)
                ),
                padding: new EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
              );
  }
}