import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  CustomButton({this.key, this.text, this.textcolor, this.buttonwidth, this.opaque, this.onPressed}) : super(key: key);
  final Key key;
  final String text;
  final Color textcolor;
  final ButtonWidth buttonwidth;
  final bool opaque;
  final VoidCallback onPressed;
  final Color blue = Color.fromARGB(0xFF, 0x6A, 0xAC, 0xFF);
  final Color alpha = Colors.white.withOpacity(0.3);
  

  @override
  Widget build(BuildContext context) {
    return new ConstrainedBox(
      constraints: BoxConstraints.expand(height: 50.0, 
      width: buttonwidth == ButtonWidth.long ? 360.0 : 170.0),
      child: new RaisedButton(
        color: opaque ? blue : alpha,
          child: new Text(text, style: new TextStyle(color: textcolor, fontSize: 20.0, fontFamily: 'OpenSans-Bold')),
          shape: new RoundedRectangleBorder(
            side: BorderSide(color: opaque ? blue : Colors.white),
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          onPressed: onPressed),
    );
  }
}

enum ButtonWidth {
  short,
  long,
}