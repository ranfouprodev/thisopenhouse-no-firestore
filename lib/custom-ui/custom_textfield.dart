import 'package:flutter/material.dart';

class CustomTextfield extends StatelessWidget {
  CustomTextfield({this.key, this.text, this.textcolor}) : super(key: key);
  final Key key;
  final String text;
  final Color textcolor;

  @override
  Widget build(BuildContext context) {
    return new Container(
    width: 360.0,
    child: 
    new TextField(    
    textAlign: TextAlign.center,
      
    style: new TextStyle(
      fontSize: 20.0,
      height: 0.5,
      fontFamily: 'OpenSans-Regular',
      color: textcolor
    ),

    decoration: 
    new InputDecoration(
      fillColor: Colors.white.withOpacity(0.3),
      hintText: text,
      filled: true,
      
    hintStyle:
    new TextStyle(
      color: textcolor,
      fontSize: 20.0,
      fontFamily: 'OpenSans-Bold'
    ), 

      border: 
      new OutlineInputBorder(

        borderSide: new BorderSide(width: 1.0, color: Colors.white),
        
      borderRadius: BorderRadius.all(Radius.circular(32.0)
      

      )
     )  
    ),
   ),
  );
 }

}