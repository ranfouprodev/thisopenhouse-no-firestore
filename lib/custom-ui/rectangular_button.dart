import 'package:flutter/material.dart';

class RectangularButton extends StatelessWidget {
  RectangularButton({this.key, this.text, this.onPressed, this.opaque, this.asset}) : super(key: key);
  final Key key;
  final String text;
  final VoidCallback onPressed;
  final bool opaque;
  final GraphicAsset asset;

  final Color blue = Color.fromARGB(0xFF, 0x6A, 0xAC, 0xFF);
  final Color alpha = Colors.white.withOpacity(0.3);
  

  @override
  Widget build(BuildContext context) {
    return new ConstrainedBox(
      constraints: BoxConstraints.expand(height: 200.0, width: 400.0),
      child: new RaisedButton(

          color: opaque ? blue : alpha,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children:
            [
            getImage(asset),
            new Container(margin: const EdgeInsets.only(bottom: 7.45)),
            Text(text, style: new TextStyle(color: Colors.white, fontSize: 20.0, fontFamily: 'OpenSans-Bold')),
            ]
          ),
          shape: new RoundedRectangleBorder(
            side: BorderSide(color: opaque ? blue : Colors.white),
              borderRadius: BorderRadius.all(Radius.circular(5.0)
              ),
              
              ),
          onPressed: onPressed),
    );
  }
  Image getImage(GraphicAsset select){
    switch(select){
      case GraphicAsset.checkmark:{
      return Image.asset("assets/ICON-CHECKMARK.png");
      }
      break;

      case GraphicAsset.chain:{
      return Image.asset("assets/ICON-CHAIN.png");
      }
      break;

      case GraphicAsset.gear:{
      return Image.asset("assets/ICON-GEAR.png");
      }
      break;

      case GraphicAsset.graph:{
      return Image.asset("assets/ICON-GRAPH.png");
      }
      break;

      case GraphicAsset.hourglass:{
      return Image.asset("assets/ICON-HOURGLASS.png");
      }
      break;

      case GraphicAsset.lightbulb:{
      return Image.asset("assets/ICON-LIGHTBULB.png");
      }
      break;

      case GraphicAsset.logo:{
      return Image.asset("assets/ICON-LOGO.png");
      }
      break;

      case GraphicAsset.upload:{
      return Image.asset("assets/ICON-UPLOAD.png");
      }
      break;

      case GraphicAsset.none:{
      return null;
      }
      break;

      default: {
        return null;
      }

    }

    
  }
}
enum GraphicAsset {
  chain,
  checkmark,
  gear,
  graph,
  hourglass,
  lightbulb,
  logo,
  upload,
  none
}