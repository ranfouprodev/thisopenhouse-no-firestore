import 'package:flutter/material.dart';

import '../screens/title.dart';
import '../screens/login.dart';
import '../screens/learn_more.dart';
import '../screens/signup.dart';
import '../screens/forgot_password.dart';
import '../screens/main_overview.dart';
import '../screens/settings.dart';
import '../screens/theme_settings.dart';
import '../screens/new_open_house.dart';
import '../screens/realtor_picture.dart';
import '../screens/brokerage_logo.dart';
import '../screens/analytics.dart';
import '../screens/go_live.dart';

class Routes {
  Route<RouteSettings> routes(RouteSettings settings){
    switch (settings.name){
      case '/': return MaterialPageRoute(
          builder: (context) {
            return TitleScreen();  
          });
     case '/login': return MaterialPageRoute(
          builder: (context) {
            return LoginScreen();  
          });
      case '/learn_more': return MaterialPageRoute(
          builder: (context) {
            return LearnMoreScreen();  
          });
      case '/signup': 
      return MaterialPageRoute(
          builder: (context) {
            return SignupScreen();  
          });
      case '/forgot_password': 
      return MaterialPageRoute(
          builder: (context) {
            return ForgotPasswordScreen();  
          });
      case '/main_overview': 
      return MaterialPageRoute(
          builder: (context) {
            return MainOverviewScreen();  
          });
      case '/settings_main': 
      return MaterialPageRoute(
          builder: (context) {
            return SettingsScreen();  
          });
      case '/theme_settings': 
      return MaterialPageRoute(
          builder: (context) {
            return ThemeSettingsScreen();  
          });
      case '/profile': 
      return MaterialPageRoute(
          builder: (context) {
            return NewOpenHouseScreen();  
          });
      case '/realtor': 
      return MaterialPageRoute(
          builder: (context) {
            return RealtorPictureScreen();  
          });
      case '/brokerage': 
      return MaterialPageRoute(
          builder: (context) {
            return BrokerageLogoScreen();  
          });
      case '/analytics': 
      return MaterialPageRoute(
          builder: (context) {
            return AnalyticsScreen();  
          });
      case '/live': 
      return MaterialPageRoute(
          builder: (context) {
            return GoLiveScreen();  
          });
    }
    return null;
  }
}