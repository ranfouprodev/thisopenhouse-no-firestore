import 'dart:async';


class Bloc extends Object {

  final _email = StreamController<String>();
  final _password = StreamController<String>();

    Function(String) get watchEmail => _email.sink.add;
    Function (String) get watchPassword => _password.sink.add;
    
    Stream<String> get emailStream => _email.stream;
    Stream<String> get passwordStream => _password.stream;







}